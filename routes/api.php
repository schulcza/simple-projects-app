<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('project',App\Http\Controllers\ProjectController::class)->only([
	'index','store','show','update','destroy'
]);

Route::get('statuses', [App\Http\Controllers\ProjectController::class, 'getStatuses']);
Route::post('/contact/remove/{id}', [App\Http\Controllers\ProjectController::class, 'removeContact']);
