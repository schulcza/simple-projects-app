<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Status Language Lines
    |--------------------------------------------------------------------------
    */

	'STATUS_WAITING'			=> 'Waiting for development',
	'STATUS_IN_PROGRESS'		=> 'In progress',
	'STATUS_FINISHED'			=> 'Finished'
];