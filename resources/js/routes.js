const ProjectList = () =>
    import ('./components/project/List.vue')
const ProjectCreate = () =>
    import ('./components/project/Add.vue')
const ProjectEdit = () =>
    import ('./components/project/Edit.vue')

export const routes = [{
        name: 'projectList',
        path: '/',
        component: ProjectList
    },
    {
        name: 'projectEdit',
        path: '/project/:id/edit',
        component: ProjectEdit
    },
    {
        name: 'projectAdd',
        path: '/project/add',
        component: ProjectCreate
    }
]