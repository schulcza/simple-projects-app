@component('mail::message')
Project changed!

<b>New project details:</b>
@foreach ($changes as $key => $change)
<p>
	{{ ucfirst($key) }}: {{ $key == 'status' ? getStatusName($change) : $change}}
</p>
@endforeach

@endcomponent
