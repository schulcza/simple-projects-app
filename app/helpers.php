<?php

use App\Models\Page;

if (!function_exists('getAvailableStatuses')) {
	/**
	 * Get model's available statuses
	 *
	 * @param string $name
	 * @return string
	 */
    function getAvailableStatuses($modelName, $sorting = true): ?array {
		$model = 'App\\Models\\' . $modelName;
		$constantsArray = [];

		if(defined('App\\Models\\' . $modelName . '::AVAILABLE_STATUSES')) {
			foreach($model::AVAILABLE_STATUSES as $key => $constant) {
				$constantsArray[$constant] = getStatusName($constant);
			}
		}

		if($sorting) {
			asort($constantsArray);
		}
		return $constantsArray;
    }
}

if (!function_exists('getStatusName')) {
	/**
	 * Get status name by status code
	 *
	 * @param integer $status
	 * @return string
	 */
    function getStatusName(int $status): string {
		return __('status.' . \App\Classes\Status::getName($status));
    }
}