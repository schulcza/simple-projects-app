<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Models\Project;
use App\Models\ProjectContact;

use App\Mail\ProjectChangedMail;

use App\Http\Resources\ProjectResource;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();

        return response()->json(ProjectResource::collection($projects));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validated = $request->validate([
			'name' 			=> ['required', 'min:3'],
			'description' 	=> ['required'],
			'status' 		=> ['required'],
			'contacts' 		=> ['array'],
		]);

        $project = Project::create($validated);

		$this->updateContacts($validated['contacts'], $project);

        return response()->json(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return response()->json(new ProjectResource($project));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
		$validated = $request->validate([
			'name' 			=> ['required', 'min:3'],
			'description' 	=> ['required'],
			'status' 		=> ['required'],
			'contacts' 		=> ['array'],
		]);

		$project->update($validated);

		$this->updateContacts($validated['contacts'], $project);

		if($project->wasChanged()) {
			foreach ($project->contacts as $key => $contact) {
				Mail::to($contact->email)->send(new ProjectChangedMail($project->getChanges()));
			}
		}

        return response()->json(200);
    }

	public function updateContacts($contacts, $project) 
	{
		foreach ($contacts as $key => $contact) {
			if(array_key_exists('id', $contact)) {
				ProjectContact::find($contact['id'])
								->update($contact);
			} else {
				$contact['project_id'] = $project->id;
				ProjectContact::create($contact);
			}
		}
	}
	
	public function removeContact($id) 
	{
		ProjectContact::find($id)->delete();
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();

        return response()->json(200);
    }

	/**
	 * Get available statuses for the project
	 * @return \Illuminate\Http\Response
	 */
	public function getStatuses() {
		$statuses = getAvailableStatuses('Project');

		return response()->json($statuses);
	}
}
