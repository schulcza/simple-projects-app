<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProjectChangedMail extends Mailable
{
    use Queueable, SerializesModels;

	public $changes;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $changes)
    {
        $this->changes = $changes;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.project-changed');
    }
}
