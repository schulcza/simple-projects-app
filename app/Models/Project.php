<?php

namespace App\Models;

use App\Classes\Status;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	/**
     * The available status codes.
     *
     * @var array
     */
    const AVAILABLE_STATUSES = [
		Status::STATUS_WAITING,
		Status::STATUS_IN_PROGRESS,
		Status::STATUS_FINISHED
	];

    protected $fillable = [
		'name',
		'description',
		'status'
	];

	public function contacts() {
		return $this->hasMany(ProjectContact::class);
	}
}
