<?php

namespace App\Classes;

use Illuminate\Support\Str;

class Status {
	const STATUS_WAITING = 10;
	const STATUS_IN_PROGRESS = 20;
	const STATUS_FINISHED = 30;

	static function getStatuses(): array {
		$reflection = new \ReflectionClass(__CLASS__);
		$constants = $reflection->getConstants();

		return array_filter($constants, function($key) {
			return Str::startsWith($key, 'STATUS_');
		}, ARRAY_FILTER_USE_KEY);
	}

	static function getCodes(): array {
		return array_flip(self::getStatuses());
	}

	static function getName(int $status_code): ?string {
		return self::getCodes()[$status_code] ?? null;
	}
}
